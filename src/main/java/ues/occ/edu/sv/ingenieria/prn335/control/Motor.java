/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.control;

import java.util.ArrayList;



import ues.occ.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author Esperanza
 */
public class Motor {

    ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();
   
    public Motor() {
        sucursales.add(new Sucursal(1, "Cinepolis Metrocentro", "Santa Ana", "Santa Ana", "Juan Perez", true));
        sucursales.add(new Sucursal(2, "Cinepolis La Gran Via", "Antiguo Cuscatlan", "San Salvador", "Luis Lopez", true));
        sucursales.add(new Sucursal(3, "Cinepolis Metrocento SM", "San Miguel", "San Miguel", "Will Salgado", false));
        sucursales.add(new Sucursal(4, "Cinemark Metrocentro", "Soyapango", "San Salvador", "Encargado 1", false));
        sucursales.add(new Sucursal(5, "Cinepolis Metrocentro Sonso", "Sonsonate", "Sonsonate", "Encargado 2", true));
        sucursales.add(new Sucursal(6, "Cine La Union", "Puerto de la union", "La Union", "Encargado 3", true));
        sucursales.add(new Sucursal(7, "Cine La Union 2", "Ciudad de la union", "La Union", "Encargado 3", false));
    }
  
public ArrayList<Sucursal> mostrarSucursales() {return sucursales;}
 

/**
 * Modifica una sucursal existente que se encuentre Inactiva
 * Reemplaza los antiguos valores con los nuevos parámetros
 * 
 * @param id_sucursal
 * @param nombre
 * @param ciudad
 * @param departamento
 * @param contacto
 * @param estado
 * @return
 * @throws IllegalArgumentException 
 */
public Boolean modificarSucursal(int id_sucursal, String nombre, String ciudad, String departamento, String contacto, boolean estado)throws IllegalArgumentException{
    if(!sucursales.get(id_sucursal-1).estado){
        sucursales.get(id_sucursal-1).setNombre(nombre);
        sucursales.get(id_sucursal-1).setCiudad(ciudad);
        sucursales.get(id_sucursal-1).setDepartamento(departamento);
        sucursales.get(id_sucursal-1).setContacto(contacto);
        sucursales.get(id_sucursal-1).setEstado(estado);
    }
    else{
        throw new IllegalArgumentException("Imposible editar, la sucursal está activa");
    }
    return true;
}


/**
 * Busca las sucursales activas por Departamento
 * Omite las sucursales cuyo Contacto es will Salgado
 * 
 * @param departamento
 * @return
 * @throws IllegalArgumentException 
 */
public ArrayList<Sucursal> buscarActivos(String departamento) throws IllegalArgumentException{
    ArrayList<Sucursal> lista= new ArrayList();
    int tamanio=sucursales.size();
    if (!departamento.isEmpty()) {
        
        //Recorrido para recuperar la lista filtrada
        for (int numero = 0; numero < tamanio; numero++) {
            sucursales.get(numero).getDepartamento();
            if (sucursales.get(numero).getDepartamento().equalsIgnoreCase(departamento)) {
                if (!sucursales.get(numero).getContacto().equalsIgnoreCase("Will Salgado")) {
                    lista.add(sucursales.get(numero));
                }
            }
        }
        return lista;
    } else{
        throw new IllegalArgumentException("Por favor, ingrese un departamento");
    }
}





}
