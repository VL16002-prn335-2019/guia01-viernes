/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.boundary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ues.occ.edu.sv.ingenieria.prn335.control.Motor;
import ues.occ.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author wayne
 */
@WebServlet(name = "FrmServlet", urlPatterns = {"/FrmServlet"})
public class FrmServlet extends HttpServlet {

    Motor motor=new Motor();
    boolean estado;   
    ArrayList<Sucursal> lista = new ArrayList();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Editar</title>");            
            out.println("</head>");
            out.println("<body>");
            
            /**
             * Acción presionar botón "Editar"
             */
            if(request.getParameter("editar")!=null){
               
                
                if(request.getParameter("id").isEmpty() || request.getParameter("nombre").isEmpty() || request.getParameter("ciudad").isEmpty() || request.getParameter("departamento").isEmpty() || request.getParameter("contacto").isEmpty()){
                    out.println("<h1>Por favor llene todos los espacios</h1>");
                }
                else{
                //Recuperamos el estado de la sucursal en una variable tipo boolean
                if (request.getParameter("estado").equalsIgnoreCase("Activo")) {
                    estado = true;
                } else {
                    estado = false;
                }
                
                
                try {
                    /**
                     * Llamada al método modificarSucursal()
                     */
                    motor.modificarSucursal(Integer.parseInt(request.getParameter("id")), request.getParameter("nombre"), request.getParameter("ciudad"), request.getParameter("departamento"), request.getParameter("contacto"), estado);

                    int tamanio = motor.mostrarSucursales().size();
                    
                    out.println("<h1>Sucursales</h1>");
                    out.println("<table border=1>");
                    out.println("<tr>");
                    out.println("<th>ID</th>");
                    out.println("<th>Nombre</th>");
                    out.println("<th>Ciudad</th>");
                    out.println("<th>Departamento</th>");
                    out.println("<th>Contacto</th>");
                    out.println("<th>Estado</th>");
                    out.println("</tr>");
                    int numero = 0;
                    
                    /**
                     * Imprimir los valores del ArrayList
                     */
                    while (tamanio != 0) {
                        out.println("<tr>");
                        out.println("<td>" + motor.mostrarSucursales().get(numero).getId_sucursal() + "</td>");
                        out.println("<td>" + motor.mostrarSucursales().get(numero).getNombre() + "</td>");
                        out.println("<td>" + motor.mostrarSucursales().get(numero).getCiudad() + "</td>");
                        out.println("<td>" + motor.mostrarSucursales().get(numero).getDepartamento() + "</td>");
                        out.println("<td>" + motor.mostrarSucursales().get(numero).getContacto() + "</td>");
                        if (motor.mostrarSucursales().get(numero).isEstado() == true) {
                            out.println("<td>Activo</td>");
                        } else {
                            out.println("<td>Inactivo</td>");
                        }
                        out.println("</tr>");
                        tamanio--;
                        numero++;
                    }
                    out.println("</table>");
                } catch (IllegalArgumentException e) {
                    out.println("<h1>" + e.getMessage() + "</h1>");
                }
              
                
            }    
            }
            /**
             * Acción presionar botón "Filtrar"
             */
            else if (request.getParameter("filtrar") != null) {
                out.println("<h1>Filtrar</h1>");
                
                //Try realizar el proceso, a excepción que el parámetro esté vacío
                try {
                    lista = motor.buscarActivos(request.getParameter("departamento"));
                    int tamanio = lista.size();

                    if (lista.isEmpty()) {
                        out.println("<h1>No hay coincidencias</h1>");
                    } else {

                        out.println("<table border=1>");
                        out.println("<tr>");
                        out.println("<th>ID:</th>");
                        out.println("<th>Nombre</th>");
                        out.println("<th>Ciudad</th>");
                        out.println("<th>Departamento</th>");
                        out.println("<th>Contacto</th>");
                        out.println("<th>Estado</th>");
                        out.println("</tr>");
                        int numero = 0;

                        /**
                         * Imprimir los valores del ArrayList
                         */
                        while (tamanio != 0) {
                            out.println("<tr>");
                            out.println("<td>" + lista.get(numero).getId_sucursal() + "</td>");
                            out.println("<td>" + lista.get(numero).getNombre() + "</td>");
                            out.println("<td>" + lista.get(numero).getCiudad() + "</td>");
                            out.println("<td>" + lista.get(numero).getDepartamento() + "</td>");
                            out.println("<td>" + lista.get(numero).getContacto() + "</td>");
                            if (lista.get(numero).isEstado() == true) {
                                out.println("<td>Activo</td>");
                            } else {
                                out.println("<td>Inactivo</td>");
                            }
                            out.println("</tr>");
                            tamanio--;
                            numero++;
                        }
                        out.println("</table>");

                    }
                } catch (IllegalArgumentException e) {
                    out.println("<h1>" + e.getMessage() + "</h1>");
                }

            }
            out.println("</body>");
            out.println("</html>");
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
